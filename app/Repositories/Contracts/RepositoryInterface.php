<?php


namespace App\Repositories\Contracts;


use App\Exceptions\RecordCreationException;
use App\Exceptions\RecordUpdateException;

interface RepositoryInterface
{
    /**
     * Model must be set
     *
     * @return void
     */
    public function setModel();

    /**
     * @return mixed
     */
    public function getFields();

    /**
     * @param $data
     * @return mixed
     * @throws RecordCreationException
     */
    public function create($data);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $id
     * @param array $data
     * @return mixed
     * @throws RecordUpdateException
     */
    public function update($id, $data);

    /**
     * @param $id
     * @return mixed
     */
    public function get($id);

    /**
     * @return mixed
     */
    public function all();

}
