<?php


namespace App\Repositories;


use App\Exceptions\RecordCreationException;
use App\Exceptions\RecordUpdateException;
use App\Repositories\Contracts\RepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class Repository implements RepositoryInterface
{

    /**
     * @var Model $model
     */
    protected $model;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        // Model must be set before execute the repository
        $this->setModel();
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->model->getFillable();
    }

    /**
     * @param $data
     * @return mixed
     * @throws RecordCreationException
     */
    public function create($data)
    {
        $model = $this->model->create($data);

        if (is_string($model)) {
            throw new RecordCreationException($model);
        }
        return $model;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        try {
            return $this->model->destroy($id);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     * @throws RecordUpdateException
     */
    public function update($id, $data)
    {
        $record = $this->get($id);
        $model  = $record->update($data);

        if (is_string($model)) {
            throw new RecordUpdateException($model);
        }
        return $model;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        try {
            return $this->model->find($id);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function all()
    {
        try {
            return $this->model->all();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
