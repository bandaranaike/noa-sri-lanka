<?php


namespace App\Repositories;


use App\Model\Contact;
use App\Repositories\Contracts\ContactRepositoryInterface;

class ContactRepository extends Repository implements ContactRepositoryInterface
{

    /**
     * Model must be set
     *
     * @return void
     */
    public function setModel()
    {
        $this->model = new Contact();
    }
}
