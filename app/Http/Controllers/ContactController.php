<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use \App\Repositories\Contracts\ContactRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct(ContactRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }


    public function create()
    {
        return view('contact.create');
    }


    public function store(ContactRequest $request)
    {
        $resp = $this->createRecord($request);
        return new JsonResponse($resp);
    }
}
