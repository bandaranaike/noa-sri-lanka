<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TimeLineEventController extends Controller
{
    public function show()
    {
        return view('timeline.show');
    }
}
