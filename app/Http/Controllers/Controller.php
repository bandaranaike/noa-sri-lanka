<?php

namespace App\Http\Controllers;

use App\Exceptions\FileUploadFailException;
use App\Exceptions\RecordCreationException;
use App\Exceptions\RecordUpdateException;
use App\Helpers\FileUploader;
use App\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * @return Collection|Model[]
     */
    public function index()
    {
        return $this->repository->all();
    }

    /**
     * Returning the item in json format
     *
     * @param $id
     * @return JsonResponse
     */
    public function getItem($id)
    {
        return new JsonResponse($this->repository->get($id));
    }

    /**
     * Returning all the list in json format
     *
     * @return JsonResponse
     */
    public function getAllList()
    {
        return new JsonResponse($this->repository->all());
    }

    /**
     * Delete the item and returning the status in json format
     *
     * @param $id
     * @return JsonResponse
     */
    public function delete($id)
    {
        return new JsonResponse($this->repository->delete($id));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadImage(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                return new JsonResponse(FileUploader::upload($request->file('image')));
            } catch (FileUploadFailException $e) {
                return new JsonResponse($e->getMessage());
            }
        }
        return new JsonResponse('The file name should be `image`!', 400);
    }

    /**
     * @param Request $request
     * @param array $additional_data
     * @return JsonResponse
     */
    protected function createRecord(Request $request, $additional_data = [])
    {
        try {
            $data = array_merge($this->formData($request), $additional_data);
            $resp = $this->repository->create($data);
            return new JsonResponse($resp);
        } catch (RecordCreationException $e) {
            return new JsonResponse($e->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @param null $id
     * @return JsonResponse
     */
    protected function updateRecord(Request $request, $id = null)
    {
        if (is_null($id)) {
            $id = $request->input('id');
        }
        try {
            $resp = $this->repository->update($id, $this->formData($request));
            return new JsonResponse($resp);
        } catch (RecordUpdateException $e) {
            return new JsonResponse($e->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function formData(Request $request)
    {
        return $request->only($this->repository->getFields());
    }
}
