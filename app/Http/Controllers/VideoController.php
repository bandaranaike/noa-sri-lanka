<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VideoController extends Controller
{
    public function show()
    {
        return view('video-gallery.view');
    }
}
