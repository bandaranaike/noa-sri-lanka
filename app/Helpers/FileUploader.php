<?php


namespace App\Helpers;


use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class FileUploader
{
    /**
     * @param UploadedFile $file
     * @param string $path
     * @param bool $need_original_name
     * @return array|string
     */
    public static function upload(UploadedFile $file, $path = 'images', $need_original_name = false)
    {
        $original_name = $file->getClientOriginalName();


        // Preparing the unique file name
        $file_name = self::makeFileNameSEOFriendly($original_name, true);

        // Moving to the path
        $file->move(public_path($path), $file_name);

        // When need to store original name
        if ($need_original_name)
            return [$file_name, $original_name];

        return $file_name;
    }

    /**
     * @param $file_name
     * @return array
     */
    public static function extractFileName($file_name)
    {
        // Searching the word in last 5 letters after the '.'
        $position = strpos($file_name, '.', -5);

        // Get the string length
        $length = Str::length($file_name);

        // Get the file name
        $name = substr($file_name, 0, $position);

        // Get the extension (position - length ~ -4 or -5)
        $extension = substr($file_name, $position - $length);

        return [$name, $extension];
    }

    /**
     * @param $file_name
     * @param bool $make_unique
     * @return string
     */
    public static function makeFileNameSEOFriendly($file_name, $make_unique = false)
    {
        // Extract the name
        list($name, $extension) = self::extractFileName($file_name);

        // Make it unique
        if ($make_unique)
            $extension = '-' . time() . $extension;

        return Str::slug($name) . $extension;
    }
}
