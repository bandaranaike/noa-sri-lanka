<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Contacts
Route::get('contacts', 'ContactController@create');
Route::post('contacts', 'ContactController@store');

// Timeline
Route::get('portfolio', 'TimeLineEventController@show');

// Video gallery
Route::get('video-gallery', 'VideoController@show');

// About us

Route::get('about-us', function () {
    return view('about_us');
});
