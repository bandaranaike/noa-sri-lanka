@extends('template.master')
@section('content')
    <div id="home-carousel"></div>
    <div class="colored-area">
        <div class="header-content">
            <div id="home-page-heading"></div>
            <div class="container">Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac
                consectetur
                ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Cras justo odio,
                dapibus
                ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur
                et.
                Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum
                at
                eros. Praesent commodo cursus magna
            </div>
        </div>
        <svg class="polygon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
            <polygon class="svg--lg" fill="#f2f2f2" points="0,0 70,100 100,0 100,100 0,100"></polygon>
        </svg>
    </div>
    <section class="videos">
        <div class="container">
            <div id="youtube-videos"></div>
        </div>
    </section>
@endsection
