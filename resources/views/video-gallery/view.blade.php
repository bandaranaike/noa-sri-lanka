@extends('template.master')
@section('title','Video Gallery')
@section('content')
    <div class="container py-4">
        <div id="youtube-videos"></div>
    </div>
@endsection
