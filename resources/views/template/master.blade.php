<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', config('app.name'))</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<div id="page-container">
    <div id="content-wrap">
        <div id="menu"></div>
        @yield('content')
    </div>
    <section class="footer" id="footer">
        <svg class="polygon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
            <polygon class="svg--lg" fill="#367" points="0,100 70,0 100,100 100,0 0,0"></polygon>
        </svg>
        <div class="footer-content">
            <div class="container text-center"> All rights reserved. &copy; Nippon Origami Association Sri Lanka - 2019 </div>
        </div>
    </section>
</div>
<input id="base-url" value="{{url('')}}" type="hidden"/>
<script>
    var base_url = document.querySelector('#base-url').value
</script>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
