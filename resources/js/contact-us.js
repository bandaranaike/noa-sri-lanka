import React from 'react';
import ReactDOM from 'react-dom';

import ContactForm from './components/ContactForm';
ReactDOM.render(<ContactForm/>, document.querySelector('#contact-form'));
