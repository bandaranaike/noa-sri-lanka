import React from 'react';
import ReactDOM from 'react-dom';

import MainMenu from './components/Menu';

ReactDOM.render(<MainMenu/>, document.querySelector('#menu'));

import './components/Carousel'
import './components/ContactForm'
import './components/HomePageHeading'
import './components/TimeLine'
import './components/YouTubeVideos'
