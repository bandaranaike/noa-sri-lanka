import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';
import {Container} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Hidden from "@material-ui/core/Hidden";
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import makeStyles from "@material-ui/core/styles/makeStyles";
import Divider from "@material-ui/core/Divider";

function HideOnScroll(props) {
    const {children, window} = props;
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({target: window ? window() : undefined});

    return (
        <Slide appear={false} direction="down" in={!trigger}>
            {children}
        </Slide>
    );
}

const useStyles = makeStyles(theme => ({
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: '0 8px',
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
}));

HideOnScroll.propTypes = {
    children: PropTypes.element.isRequired,
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window: PropTypes.func,
};


export default function MainMenu(props) {
    const classes = useStyles();

    const [state, setState] = React.useState({
        is_drawer_open: false,
    });

    function openDrawer() {
        setState({is_drawer_open: true})
    }

    function closeDrawer() {
        setState({is_drawer_open: false})
    }

    return (
        <React.Fragment>
            <CssBaseline/>
            <HideOnScroll {...props}>
                <AppBar color='inherit'>
                    <Toolbar>
                        <Container>
                            <Hidden smUp>
                                <IconButton
                                    color="inherit"
                                    aria-label="open drawer"
                                    edge="start"
                                    onClick={openDrawer}>
                                    <MenuIcon/>
                                </IconButton>
                            </Hidden>
                            <Hidden xsDown>
                                <Button href={base_url} color='inherit'>NOA Sri Lanka</Button>
                                <Button color="inherit" href={base_url + '/portfolio'}>Portfolio</Button>
                                <Button color="inherit" href={base_url + '/video-gallery'}>Video Gallery</Button>
                                <Button color="inherit" href={base_url + '/about-us'}>About us</Button>
                                <Button color="inherit" href={base_url + '/contacts'}>Contacts</Button>
                            </Hidden>
                        </Container>
                    </Toolbar>
                </AppBar>
            </HideOnScroll>
            <Toolbar/>
            <Drawer open={state.is_drawer_open} onClose={closeDrawer}>
                <div className={classes.drawerHeader}>
                    <IconButton onClick={closeDrawer}>
                        <ChevronLeftIcon/>
                    </IconButton>
                </div>
                <Divider/>
                <List>
                    <ListItem><Button href={base_url} color='inherit'>NOA Sri Lanka</Button></ListItem>
                    <ListItem><Button color="inherit" href={base_url + '/portfolio'}>Portfolio</Button></ListItem>
                    <ListItem><Button color="inherit" href={base_url + '/video-gallery'}>Video Gallery</Button></ListItem>
                    <ListItem><Button color="inherit" href={base_url + '/about-us'}>About us</Button></ListItem>
                    <ListItem><Button color="inherit" href={base_url + '/contacts'}>Contacts</Button></ListItem>
                </List>
            </Drawer>
        </React.Fragment>
    );
}
