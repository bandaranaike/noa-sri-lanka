import React, {Component} from "react";
import {TextField} from "@material-ui/core";
import * as ReactDom from "react-dom";
import axios from 'axios';
import Snackbar from "@material-ui/core/Snackbar";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import EmailIcon from '@material-ui/icons/EmailOutlined'
import PhoneIcon from '@material-ui/icons/Phone'
import SnackbarContent from "@material-ui/core/SnackbarContent";
import {green} from "@material-ui/core/colors";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

export default class ContactForm extends Component {
    state = {
        name: '',
        email: '',
        phone: '',
        message: '',
        success: false,
    };

    render() {
        const action = (
            <IconButton key="close" aria-label="close" color="inherit" onClick={this.closeMessage}>
                <CloseIcon/>
            </IconButton>
        );

        return (
            <Grid container className='my-5' spacing={3}>
                <Grid item xs={12} md={6}>
                    <Typography className='mb-3' variant='h3'>Contact us</Typography>
                    <Typography className='mb-2'>Send your inquiries regarding any topic. We always welcome to here from you. We will get back to you as soon as
                        possible</Typography>
                    <Typography className='mb-2'>
                        <EmailIcon/> info@noasrilanka.com <br/>
                        <EmailIcon/> noasrilanka@gmail.com
                    </Typography>
                    <Typography>
                        <PhoneIcon/> +94 719044067
                    </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                    <form onSubmit={this.handleSubmit}>
                        <TextField required fullWidth margin="normal" variant='outlined' name='name' label='Name' onChange={this.handleChange}/>
                        <TextField required fullWidth margin="normal" variant='outlined' name='email' type='email' label='Email' onChange={this.handleChange}/>
                        <TextField fullWidth margin="normal" variant='outlined' name='phone' type='text' label='Phone' onChange={this.handleChange}/>
                        <TextField required fullWidth margin="normal" variant='outlined' name='message' multiline label='Message' rowsMax={5} onChange={this.handleChange}/>
                        <Button className='mt-3' variant='contained' type='submit' onSubmit={this.handleSubmit}>Send inquiry</Button>
                    </form>
                    {this.state.success ? (
                        <SnackbarContent
                            className='mt-3'
                            style={{backgroundColor: green[600]}}
                            message='Your inquiry has been sent successfully. We will get back to you soon!'
                            action={action}
                        />) : ''}
                </Grid>
            </Grid>
        );
    }

    closeMessage = () => {
        this.setState({success: false})
    };

    handleChange = event => {
        let item = event.target.attributes.name.value;
        if (item === 'name') {
            this.setState({name: event.target.value});
        }
        if (item === 'email') {
            this.setState({email: event.target.value});
        }
        if (item === 'phone') {
            this.setState({phone: event.target.value});
        }
        if (item === 'message') {
            this.setState({message: event.target.value});
        }
    };

    handleSubmit = event => {
        event.preventDefault();
        axios.post('contacts', this.state).then(res => {
            this.setState({success: true});
        }).catch(er => {
            return (
                <Snackbar open={true} message={er.message}/>
            );
        });
    }
}

if (document.querySelector('#contact-from')) {
    ReactDom.render(<ContactForm/>, document.querySelector('#contact-from'))
}
