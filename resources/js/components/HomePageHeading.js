import React, {Component} from "react";
import {Typography} from "@material-ui/core";
import ReactDOM from "react-dom";

export default class HomePageHeading extends Component {
    render() {
        return (
            <Typography className='mb-5' variant='h2'>Nippon Origami Association Sri Lanka</Typography>
        )
    }
}


if (document.querySelector('#home-page-heading')) {
    ReactDOM.render(<HomePageHeading/>, document.querySelector('#home-page-heading'));
}
