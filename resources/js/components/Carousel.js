import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Carousel} from 'react-responsive-carousel';
import ContactForm from "./ContactForm";

export default class HomeCarousel extends Component {
    render() {
        return (
            <Carousel autoPlay showThumbs={false} showStatus={false}>
                <div>
                    <img src="images/1.png"/>
                </div>
                <div>
                    <img src="images/2.png"/>
                </div>
                <div>
                    <img src="images/3.png"/>
                </div>
                <div>
                    <img src="images/4.png"/>
                </div>
                <div>
                    <img src="images/5.png"/>
                </div>
                <div>
                    <img src="images/6.png"/>
                </div>
            </Carousel>
        );
    }
}

if (document.querySelector('#home-carousel')) {
    ReactDOM.render(<HomeCarousel/>, document.querySelector('#home-carousel'));
}
