import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Card, CardContent} from "@material-ui/core";
import CardHeader from "@material-ui/core/CardHeader";
import Grid from "@material-ui/core/Grid";
import CardMedia from "@material-ui/core/CardMedia";
import VideoLibraryIcon from "@material-ui/icons/VideoLibrary";
import ContactForm from "./ContactForm";

export default class YouTubeVideos extends Component {

    render() {
        let videos = [
            {name: 'First Video', url: 'kkk', 'description': 'Nisi euismod ante senectus consequat phasellus ut'},
            {name: 'Second Video', url: 'kkk', 'description': 'Nisi euismod ante senectus consequat phasellus ut'},
            {name: 'Third First Video', url: 'kkk', 'description': 'Nisi euismod ante senectus consequat phasellus ut'},
            {name: 'Fourth First Video', url: 'kkk', 'description': 'Nisi euismod ante senectus consequat phasellus ut'},
            {name: 'Fifth First Video', url: 'kkk', 'description': 'Nisi euismod ante senectus consequat phasellus ut'},
            {name: 'Sixth First Video', url: 'kkk', 'description': 'Nisi euismod ante senectus consequat phasellus ut'},
        ];
        return (
            <div>
                <h2 className="mb-3"><VideoLibraryIcon/> Videos</h2>
                <Grid container spacing={3}>
                    {videos.map((value, index) => {
                        return (
                            <Grid item xs={12} md={index < 2 ? 6 : 3} key={index}>
                                <Card>
                                    <CardHeader title={value.name}/>
                                    <CardMedia className='video-card-media' image={"https://picsum.photos/" + 45 + index + "/" + 30 + index} title={value.url}>

                                    </CardMedia>
                                    <CardContent>{value.description}</CardContent>
                                </Card>

                            </Grid>
                        )
                    })}
                </Grid>
            </div>
        );
    }
}
if (document.querySelector('#youtube-videos')) {
    ReactDOM.render(<YouTubeVideos/>, document.querySelector('#youtube-videos'));
}


